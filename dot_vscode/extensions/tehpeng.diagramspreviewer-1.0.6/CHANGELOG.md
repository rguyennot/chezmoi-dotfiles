# Change Log

All notable changes to the "diagramasacodepreview" extension will be documented in this file.

## [0.1.0]
- Initial release (Preview)

## [1.0.0]
- Support pan/zoom of diagram on window itself
- Save image to a location of your choice

## [1.0.1]
- Official release

## [1.0.2]
- Provide error message when file save failed

## [1.0.3]
- Update README.md - toggle preview instruction for Mac
- Fix Windows save file destination path error

## [1.0.4]
- NIL

## [1.0.5]
- New settings: Configure your own python command
- Updated README.md - on the settings configurations available

## [1.0.6]
- Support using [VS Code's python extension's interpreter](https://code.visualstudio.com/docs/python/environments#_manually-specify-an-interpreter) via introducting new option 
(`VS Code Python Interpreter`) to python command configuration
- Updated README.md - on the settings configurations available