# How to setup your Macbook workspace

### Define your default shell

By default, Terminal use `Zsh` shell but you can use another one like `Bash`, `Fish`...etc.

I love `Zsh` and `OhMyZsh` because there are many possibilities for customization !

To know your shell version, execute : 
```bash 
$SHELL --version
```
To set Zsh as your dedault shell : 
```bash
chsh -s /usr/local/bin/zsh
```

### Oh My ZSH! (*for Terminals*)

`OhMyZsh` is a delightful, open source, community-driven framework for managing your `Zsh` configuration.

To install OhMyZsh : https://ohmyz.sh/#install

### Starship (*for Prompts*)
`Starship` is a customizable prompt for any shell. It come with many plugins, specialy for `Kubernetes`, `Git`...etc.

> **Requirements** : *Nerd Fonts* installed like *Fira Code* >> https://starship.rs/guide/#prerequisites
{.is-info}

To install Starship on MacOS : 
```bash
brew install starship
```

**Configuration**
You need to create a config file into .config folder in your homedirectory : 
```bash
mkdir -p ~/.config && touch ~/.config/starship.toml
```

**Customization**

Copy/Paste it in your `~/.config/starship.toml` :
```toml
format = """\
    $username\
    $hostname\
    $directory\
    $git_branch\
    $git_commit\
    $git_state\
    $git_status\
    $hg_branch\
    $docker_context\
    $package\
    $dotnet\
    $elixir\
    $elm\
    $erlang\
    $golang\
    $java\
    $julia\
    $nim\
    $nodejs\
    $ocaml\
    $php\
    $purescript\
    $python\
    $ruby\
    $rust\
    $terraform\
    $zig\
    $nix_shell\
    $conda\
    $memory_usage\
    $aws\
    $env_var\
    $crystal\
    $kubernetes\
    $cmd_duration\
    $custom\
    $line_break\
    $jobs\
    $battery\
    $time\
    $character\
    """

[git_branch]
symbol = "🌱 "

[directory]
home_symbol = "🏠 "

[kubernetes]
disabled = false
format = '[$symbol\[ $context \]]($style) '
[kubernetes.context_aliases]
"gke_project-name-production" = "prod"
"gke_project-name-staging" = "staging"
"gke_project-name-tooling" = "tooling"
```

# Homebrew packages

Install these packages with brew CLI : 
- iterm2 (+ _FiraCode Nerd Fonts_ or _Caskaydia Cove Nerd Fonts_)
- chezmoi
- zsh
- neovim
- stern
- velero 
- starship
- helm
- kubectl
- marp-cli
- hidetatz/tap/kubecolor (colorize kubectl output commands)
- hashicorp/tap/terraform


# How to setup kubectl for GCP
`kubectl` is needed to access to our Kubernetes clusters in GCP from your laptop.

## Requirements
- ***Python 3.x***
- [gcloud CLI](https://cloud.google.com/sdk/docs/install?hl=fr)
- [kubectl](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl?hl=fr#install_kubectl)
- [gke-gcloud-auth-plugin](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl?hl=fr#install_plugin)

## How to install/configure kubectl for projects ?

**Google** documentation is your friend :) 
> https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl?hl=fr

### Set the `staging` project : 
```bash
gcloud config set project <project-name-staging>
gcloud config set compute/zone europe-west1-b
gcloud config set compute/region europe-west1
gcloud components update
gcloud container clusters get-credentials <cluster-name>
```

### Set the `production` project : 
```bash
gcloud config set project <project-name-production>
gcloud config set compute/zone europe-west1-b
gcloud config set compute/region europe-west1
gcloud components update
gcloud container clusters get-credentials <cluster-name>
```

### Set the `tooling` project :
```bash
gcloud config set project <project-name-tooling>
gcloud config set compute/zone europe-west1-b
gcloud config set compute/region europe-west1
gcloud components update
gcloud container clusters get-credentials <cluster-name>
```

## kubectl plugins (krew)

To install kubectl plugins, you need first to install krew : https://krew.sigs.k8s.io/docs/user-guide/setup/install/

To install plugins for kubectl, just run the following command : 
```bash
kubectl krew install <plugin-name>
```

## _must-have_ plugins 
- access-matrix
- ctx
- krew
- neat
- ns
- oidc-login
- view-secret
- view-serviceaccount-kubeconfig
- who-can